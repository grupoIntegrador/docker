-- atividade
CREATE SEQUENCE sequence_atividade;
CREATE TABLE atividade (
    id INTEGER PRIMARY KEY  DEFAULT nextval('sequence_atividade'),
    descricao VARCHAR,
	nome_atividade	VARCHAR,
    id_modulo INTEGER,
    id_lider_tecnico INTEGER,
    id_lider_projeto INTEGER,
    branch VARCHAR,
    fontes VARCHAR,
    scripts BOOLEAN,
    observacao VARCHAR,
    status_atividade VARCHAR,
	release Date,
	in_producao BOOLEAN
);
ALTER SEQUENCE sequence_atividade
OWNED BY atividade.id;

-- modulo
CREATE SEQUENCE sequence_modulo;
CREATE TABLE modulo (
    id INTEGER PRIMARY KEY DEFAULT nextval('sequence_modulo'),
    descricao VARCHAR,
	id_sistema INTEGER
	
);

ALTER SEQUENCE sequence_modulo
OWNED BY modulo.id;

-- sistema
CREATE SEQUENCE sequence_sistema;
CREATE TABLE sistema (
    id INTEGER PRIMARY KEY DEFAULT nextval('sequence_sistema'),
    descricao VARCHAR
);
ALTER SEQUENCE sequence_sistema
OWNED BY sistema.id;

-- USUARIO
CREATE SEQUENCE sequence_usuario;
CREATE TABLE usuario (
    id INTEGER PRIMARY KEY DEFAULT nextval('sequence_usuario'),
    usuario VARCHAR,
	usuario_ldap VARCHAR UNIQUE
);

ALTER SEQUENCE sequence_usuario
OWNED BY usuario.id;

-- atividade_dev
CREATE TABLE atividade_dev (
    id_dev INTEGER,
    id_atividade INTEGER
);

-- entrega_ambiente
CREATE TABLE entrega_ambiente (
    id_atividade INTEGER,
    id_ambiente INTEGER,
	inambiente BOOLEAN,
    data_entrega DATE
);

-- ambiente
CREATE SEQUENCE sequence_ambiente;
CREATE TABLE ambiente (
    id INTEGER PRIMARY KEY DEFAULT nextval('sequence_ambiente'),
    descricao VARCHAR
);

ALTER SEQUENCE sequence_ambiente
OWNED BY ambiente.id;


ALTER TABLE atividade ADD CONSTRAINT atividade_modulo_fk FOREIGN KEY (id_modulo) REFERENCES modulo(id);
ALTER TABLE atividade ADD CONSTRAINT atividade_usuario_fk_1 FOREIGN KEY (id_lider_tecnico) REFERENCES usuario(id);
ALTER TABLE atividade ADD CONSTRAINT atividade_usuario_fk_2 FOREIGN KEY (id_lider_projeto) REFERENCES usuario(id);
ALTER TABLE modulo ADD CONSTRAINT modulo_sistema_fk FOREIGN KEY (id_sistema) REFERENCES sistema(id);
ALTER TABLE atividade_dev ADD CONSTRAINT atividade_dev_atividade_fk FOREIGN KEY (id_atividade) REFERENCES atividade(id);
ALTER TABLE atividade_dev ADD CONSTRAINT atividade_dev_dev_fk FOREIGN KEY (id_dev) REFERENCES usuario(id);
ALTER TABLE entrega_ambiente ADD CONSTRAINT entrega_ambiente_atividade_fk FOREIGN KEY (id_atividade) REFERENCES atividade(id);
ALTER TABLE entrega_ambiente ADD CONSTRAINT entrega_ambiente_ambiente_fk FOREIGN KEY (id_ambiente) REFERENCES ambiente(id);







