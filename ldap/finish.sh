# !/bin/bash


IFS='
'

rm -R -f /tmp/ldapsearch-*

ldapsearch -LL -b "ou=Applications,dc=usersad,dc=everis,dc=int" -D "cn=admin,dc=usersad,dc=everis,dc=int" -w senha -tt "(Objectclass="groupOfNames")" member

for file in /tmp/*
do

        for i in $(cat ${file});
        do
            
			ldappasswd -D "cn=admin,dc=usersad,dc=everis,dc=int" -w senha -s senha "${i}";
        done

		rm -R -f ${file}
done